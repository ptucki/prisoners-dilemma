from itertools import combinations_with_replacement, product
from random import choice, choices, random, randrange
from statistics import mean, stdev
from typing import Dict, List, Tuple

# Represents a cooperation move in prisoner's dilemma.
MOVE_COOPERATE = 'C'
# Represents a defect move in prisoner's dilemma.
MOVE_DEFECT = 'D'
# Represents all possible moves.
MOVES = MOVE_COOPERATE, MOVE_DEFECT
# Prisoner's dilemma game rules.
GAME_RULES = {
    MOVE_COOPERATE: {
        MOVE_COOPERATE: (3, 3),
        MOVE_DEFECT: (0, 5),
    },
    MOVE_DEFECT: {
        MOVE_COOPERATE: (5, 0),
        MOVE_DEFECT: (1, 1),
    },
}

# The number of previous rounds to consider when doing a move.
HISTORY_SIZE = 2
# The number of solutions in the population in each phase.
POPULATION_SIZE = 400
# The number of rounds in iterative prisoner's dilemma.
GAME_ROUNDS = 10
# The number of search iterations to perform.
SEARCH_ROUNDS = 50
# The chance of a mutation during evolution.
MUTATION_PROBABILITY = 0.001


# Generates data structures that connect history moves to an index in solution's binary string containing next move.
# Initial strategy index is meant to be used before history of HISTORY_SIZE length is available.
# Initial strategy index is keyed by all previous opponent's move variations with repetition.
# Initial strategy index example keys: '' (before first move), '10' (before third move).
# Main strategy index is meant to be used when history of HISTORY_SIZE length is available (most of the time).
# Main strategy index is keyed by all previous player's and opponent's move variations with repetition.
# Main strategy index example keys with HISTORY_SIZE = 3: '100001', '101110' (player move, opponent move - three times).
def generate_strategy_indexes() -> Tuple[Dict[str, int], Dict[str, int]]:
    initial_histories = [''.join(history) for size in range(HISTORY_SIZE) for history in product(MOVES, repeat=size)]
    initial_histories_size = len(initial_histories)
    main_histories = [''.join(history) for history in product(MOVES, repeat=HISTORY_SIZE * 2)]
    return (
        {history: index for index, history in enumerate(initial_histories)},
        {history: index + initial_histories_size for index, history in enumerate(main_histories)},
    )


# Generates a population of specified size with random solutions of specified size.
def generate_random_population(solution_size: int, population_size: int) -> List[str]:
    return [''.join(choice(MOVES) for _ in range(solution_size)) for _ in range(population_size)]


# Generates a fitness value for each member of the population using round robin tournament versus self.
def evaluate_population_vs_self(population: List[str]) -> List[int]:
    scores = {index_a: {index_b: None for index_b in range(len(population))} for index_a in range(len(population))}
    for index_a, index_b in combinations_with_replacement(range(len(population)), 2):
        scores[index_a][index_b], scores[index_b][index_a] = play_game(population[index_a], population[index_b])
    return [mean(scores[index].values()) for index in range(len(population))]


# Generates a fitness value for each member of the population using round robin tournament versus random population.
def evaluate_population_vs_random(population: List[str]) -> List[int]:
    opponent_population = generate_random_population(len(population[0]), 100)
    scores = {index_a: {index_b: None for index_b in range(len(opponent_population))} for index_a in range(len(population))}
    for index_a in range(len(population)):
        for index_b in range(len(opponent_population)):
            scores[index_a][index_b] = play_game(population[index_a], opponent_population[index_b])[0]
    return [mean(scores[index].values()) for index in range(len(population))]


# Plays iterative prisoner's dilemma game between two solutions with specified number of rounds.
def play_game(solution_a: str, solution_b: str) -> Tuple[int, int]:
    score_a = 0
    score_b = 0
    history_a = ''
    history_b = ''
    for game_round in range(GAME_ROUNDS):
        significant_history_a = history_a[HISTORY_SIZE * -2:]
        significant_history_b = history_b[HISTORY_SIZE * -2:]
        if game_round >= HISTORY_SIZE:
            move_a = solution_a[main_strategy_index[significant_history_a]]
            move_b = solution_b[main_strategy_index[significant_history_b]]
        else:
            opponent_history_a = significant_history_a[1::2]
            opponent_history_b = significant_history_b[1::2]
            move_a = solution_a[initial_strategy_index[opponent_history_a]]
            move_b = solution_b[initial_strategy_index[opponent_history_b]]
        history_a += move_a + move_b
        history_b += move_b + move_a
        result = GAME_RULES[move_a][move_b]
        score_a += result[0]
        score_b += result[1]
    return score_a, score_b


# Evolves the population into new one given fitness values.
def evolve_population(population: List[str], population_fitness: List[int]) -> List[str]:
    scaled_population_fitness = scale_population_fitness(population_fitness)
    new_population = []
    while len(new_population) < len(population):
        parent_a = choices(population, weights=scaled_population_fitness)[0]
        parent_b = choices(population, weights=scaled_population_fitness)[0]
        child_a, child_b = crossover(parent_a, parent_b)
        if random() < MUTATION_PROBABILITY:
            child_a = mutate(child_a)
        if random() < MUTATION_PROBABILITY:
            child_b = mutate(child_b)
        new_population.append(child_a)
        new_population.append(child_b)
    return new_population


# Apply linear fitness scaling to improve search process.
def scale_population_fitness(population_fitness: List[int]) -> List[int]:
    fitness_avg = mean(population_fitness)
    fitness_min = min(population_fitness)
    a = fitness_avg / (fitness_avg - fitness_min)
    b = - fitness_min * a
    return [a * fitness + b for fitness in population_fitness]


# Crosses over two parents into two children.
def crossover(parent_a: str, parent_b: str) -> Tuple[str, str]:
    initial_length = len(initial_strategy_index)
    main_length = len(main_strategy_index)
    initial_cut_index = randrange(initial_length)
    main_cut_index = randrange(initial_length, initial_length + main_length)
    parent_a_initial, parent_a_main = parent_a[:initial_length], parent_a[initial_length:]
    parent_b_initial, parent_b_main = parent_b[:initial_length], parent_b[initial_length:]
    child_a = parent_a_initial[:initial_cut_index] + parent_b_initial[initial_cut_index:] + parent_a_main[:main_cut_index] + parent_b_main[main_cut_index:]
    child_b = parent_b_initial[:initial_cut_index] + parent_a_initial[initial_cut_index:] + parent_b_main[:main_cut_index] + parent_a_main[main_cut_index:]
    return child_a, child_b


# Flips a random bit in a solution.
def mutate(solution: str) -> str:
    mutation_index = randrange(len(solution))
    new_value = MOVE_DEFECT if solution[mutation_index] == MOVE_COOPERATE else MOVE_COOPERATE
    solution = solution[:mutation_index] + new_value + solution[mutation_index + 1:]
    return solution


# Returns the best solution from population.
def get_winner(population: List[str], population_fitness: List[int]) -> Tuple[str, int]:
    fitness_max = max(population_fitness)
    fitness_max_key = [index for index, fitness in enumerate(population_fitness) if fitness == fitness_max][0]
    return population[fitness_max_key], fitness_max


# Display human readable solution description.
def describe_solution(solution: str) -> None:
    print('Solution: ' + solution + '\n')
    i = -1
    for opponent_history, solution_index in initial_strategy_index.items():
        if len(opponent_history) > i:
            i += 1
            print('Move ' + str(i) + ':')
        if opponent_history == '':
            opponent_history = ' '
        print(f'  {opponent_history} -> {solution[solution_index]}')
    print('Move n:')
    for history, solution_index in main_strategy_index.items():
        print(f'  {history} -> {solution[solution_index]}')


initial_strategy_index, main_strategy_index = generate_strategy_indexes()
solution_length = len(initial_strategy_index) + len(main_strategy_index)
population = generate_random_population(solution_length, POPULATION_SIZE)
population_fitness = evaluate_population_vs_self(population)
for search_round in range(SEARCH_ROUNDS):
    population = evolve_population(population, population_fitness)
    population_fitness = evaluate_population_vs_self(population)
    population_fitness_standard_deviation = stdev(population_fitness)
    winner = get_winner(population, population_fitness)
    print('Round %02d: max(f) = %.2f, stddev(f) = %.2f' % (search_round + 1, winner[1], population_fitness_standard_deviation))
    if population_fitness_standard_deviation < 0.1:
        break
print('')
describe_solution(winner[0])

# Related white paper https://mpra.ub.uni-muenchen.de/28574/1/MPRA_paper_28574.pdf.
